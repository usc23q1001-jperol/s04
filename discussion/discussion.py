# Lists - square bracket ([])
names = ["John", "Paul", "George", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']
duration = [260, 180, 20]
truth_values = [True, False, True, True, False]

sample_list = ["Apple", 3, False, "Potato", 4, True]
print(sample_list)

# Getting the list size
print(len(programs))

# Accessing values

# Access the first item in the list
print(programs[0]) #developer career

# Access the last item in the list
print(programs[-1]) #short courses

# Access the second item in the list
print(duration[1]) #180

# Access the whole list
print(duration)

# Access a range of values
# list [start index: end index]
print(programs[0:2])

# Updating
# Print the
print(f'Current value: {programs}')

#Mini Exercise 1
students = ["John", "Paul", "George", "Ringo", "Albus"]
grades = [97, 99, 96, 69, 92]
for x, y in zip(students, grades):
	print(f'The grade of {x} is {y}')

# List Manipulation
# List has methods than can be used to manipulate the elements within

# Adding List items - the append() method allows to insert items to a list
programs.append('global')
print(programs)

# Deleting List items - the 'del' keyword can be used ti delete elements in the lsit
duration.append(360)
print(duration)

# Delete the last item
del duration[-1]
print(duration)

# Memebership checks - the "in" keyword checks if the element is in the lsit
# returns true or false
print(20 in duration)
print(500 in duration)

# Sorting lists - the sort() method sorts the list alphanumerically, ascneding by default
print(names)
names.sort()
print(names)

# Emptying the list - the clear() method is used to empty the contents of the list
test_list = [1,3,5,7,9]
print(test_list)
test_list.clear()
print(test_list)

# Dictionaries are used to store data values in key:value pairs
# To create a dictionary, the curly braces({}) is used and the 
# key-value pairs are denoted with (key:value)
person1 = {
	"name": "Nikolai",
	"age": 18,
	"subjects": ["Python", "SQL", "Django"]
}
print(person1)

# To get the number of key-value pairs in the dic, use len() method
print(len(person1))

# Accessing values in the dic
print(person1["name"])

# The keys() method will return a list of all keys in the dic
print(person1.keys())

# The values() method will return a list of all values in the dic
print(person1.values())

# The items() method will return each item in a dic as a key-value pair
print(person1.items())

# Adding key-value pairs can be done either putting a new index key and
# assigning a value or the update() method
person1["nationality"] = "Filipino"
person1.update({"fave_food": "Sinigang"})
print(person1)

# Deleting entries can be done using pop() method or del 
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# clear
person2 = {
	"name": "John",
	"age": 25
}

print(person2)
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nested dictionaries
person3 = {
	"name": "Monika",
	"age": 20
}

# Mini-Exercise 2
car1 = {
	"brand": "Toyota",
	"model": "Corolla",
	"year_of_make": 2022,
	"color": "Black"
}
print(f'I own a {car1["brand"]} {car1["model"]} and it was made in {car1["year_of_make"]}')

# Functions - blocks of code that runs when called
# The "def" keyword is used to create a function
# def <functionName>()

# define a function called my_greeting
def my_greeting():
	print("Hello User")

# Calling/Invoking a function
my_greeting()

# Parameters can be added to function to have more control to inputs
def greet_user(username):
	# prints out the value of the username
	print(f'Hello, {username}!')

# Arguments are the values that are substituted to the parameters
greet_user("Bob")
greet_user("Amy")

# From a functions perspecitve:
# A parameter is a variable listed inside the () in the function definition
# Argument is the value that is sent to the function when it is called

# return statement - the "return" keyword allow functions to return values
def addition(num1, num2):
	return num1 + num2

sum = addition(1, 2)
print(f"The sum is {sum}.")

# Lambda Functions - small anonymous function that can be used for callbacks.
greeting = lambda person : f'hello {person}'
print(greeting("Elsie"))
print(greeting("Anthony"))

multiply = lambda a, b : a * b
print(multiply(5,6))
print(multiply(6,99))

# Classes - serve as blueprints to describe the concepts of objects
# class ClassName():

class Car(object):
	# properties
	def __init__(self, brand, model, year_of_make):
		super(Car, self).__init__()
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
		self.fuel = "Gasoline"
		self.fuel_level = 20

	# methods
	def fill_fuel(self):
		print(f'Current fuel level: ')
	
	def drive(self, distance):
		
# 
new_car = Car("Nissan", "GT-R", "2019")

# Displaying attributes can be done using
print(f"My car is a {new_car.brand} {new_car.model}")

# Calling methods of the instance
new_car.fill_fuel()
new_car.drive(50)