class Camper(object):
	# properties
	def __init__(self, name, batch, course_type):
		super(Camper, self).__init__()
		self.name = name
		self.batch = batch
		self.course_type = course_type

	# methods
	def career_track(self):
		print(f'Currently enrolled in the {self.course_type} program')
	
	def info(self):
		print(f'My name is {self.name} of batch {self.batch}')
		
# object
zuitt_camper = Camper("Jian", "2023", "python")

print(f"Camper Name: {zuitt_camper.name}\nCamper Batch: {zuitt_camper.batch}\nCamper Course: {zuitt_camper.course_type}")

zuitt_camper.info()
zuitt_camper.career_track()
